# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 15:10:47 2019

@author: ov837475
"""

import numpy as np

def gradient_4point(f, dx):
    "The gradient of array f assuming points are a distance dx apart"
    "using 4-point differences"

    # Catch exception 
    if dx < 0:
        raise Exception("Distance dx should be > 0, check dy"
                        "in geostrophicWind")
        
    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f, dtype=float)
    
    # Four point differences at the end points
    dfdx[0] = (f[1] - f[0]) / dx                   #forward difference
    
    dfdx[1] = (f[2] - f[1]) / dx                   #forward difference
    
    dfdx[-1] = (f[-1] - f[-2]) / dx                #backward difference
    
    dfdx[-2] = (f[-2] - f[-3]) / dx                #backward difference
    
                     
    # Centred differences for mid-points 
    for i in range(2, len(f)-2):
        
        dfdx[i] = (f[i-2] - 8*f[i-1] + 8*f[i+1] - f[i+2]) / (12*dx)
        
    return dfdx

