# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 14:29:27 2019

@author: ov837475
"""

import numpy as np


# Functions for calculating gradients

def gradient_2point(f, dx):
    "The gradient of array f assuming points are a distance dx apart"
    "using 2-point differences"
    
    # Catch exception 
    if dx < 0:
        raise Exception("Distance dx should be > 0, check dy"
                        "in geostrophicWind")
    
    # Initialise the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f, dtype=float)
    
    # Two point differences at the end points
    dfdx[0] = (f[1] - f[0]) / dx                  # forward difference   
     
    dfdx[-1] = (f[-1] - f[-2]) / dx               # backward difference
    
    
    # Centred differences for mid-points
    for i in range(1, len(f)-1):
        
        dfdx[i] = (f[i+1] - f[i-1]) / (2*dx)
        
    return dfdx


