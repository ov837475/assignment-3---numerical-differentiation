# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 15:10:47 2019

@author: ov837475
"""

import numpy as np 
import matplotlib.pyplot as plt
from fourthorderdifferentiate import *

def fourthordergeostrophicWind ():
    "Calculate geostrophic wind using fourth-order, four-point"
    "point finite difference approximation and compare with "
    "analytic solutions"
    
    # Input parameters describing the problem 
    import geoParameters as gp
    
    
    # Resolution 
    N  = 10                    # the number of intervals to divide space into
    dy = (gp.ymax - gp.ymin)/N # the length of spacing 
    
    
    # The spatial dimension, y:
    y = np.linspace(gp.ymin, gp.ymax, N+1)
    
    # The geostrophic wind calculated using the analytic gradient
    uExact = gp.uExact(y)
    
    
    # The pressure at y points
    p = gp.pressure(y)
    
    
    # The pressure gradient and wind using four point differences 
    dpdy = gradient_4point(p, dy)
    u_4point = gp.geoWind(dpdy)

 #---------------------------------------------------------------------------
    
    # Graph to compare the numerical and analytic solutions
    # Plot using large fonts
    font = {'size' : 14}
    plt.rc('font', **font)
    
    # Plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, 'k-', label='Exact')
    plt.plot(y/1000, u_4point, '*k--', label='Four-point differences',\
             ms =12, markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.title('Plot to compare numerical and analytic solutions')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('plots/geoWindCentfourthorder.pdf')
    plt.show()
    
    #Errors between numerical and analytic solutions
    errors = uExact - u_4point
     
    # Graph of the errors 
    plt.semilogy(y/1000, abs(errors), '*k--')
    plt.title('Plot of errors')
    plt.xlabel('y (km)')
    plt.ylabel('Absolute value of errors')
    plt.tight_layout()
    plt.savefig('plots/geoWindCentfourpointerrors.pdf')
    plt.show()
    
    
if __name__ == "__main__" :
    fourthordergeostrophicWind()
