# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 14:03:18 2019

@author: ov837475
"""

import numpy as np 

# Parameters for calculating the geostrophic wind

pa = 1e5                          # the mean pressure
pb = 200                          # the magnitude of the pressure variations 
f = 1e-4                          # the Coriolis force
rho = 1.0                         # density
L = 2.4e6                         # length scale of the pressure variations (k)
ymin = 0.0                        # minimum space dimension
ymax = 1e6                        # maximum space dimension (k)

def pressure(y):
    "The pressure and given y locations"
    
    # Check ymax to return positive values of u within domain 
    if ymax > 1e6:
        raise ValueError('Argument ymax to geostrophicWind should be <= 1e6')
    else:
        return pa + pb * np.cos(y*np.pi/L)
    
def uExact(y):
    "The analytic geostrophic wind at given locations, y"
    
    # Make assertion on one parameter 
    assert (pb == 200), "pb should be 200, uExact must return correct values"
    
    return pb * np.pi/(rho*f*L) * np.sin(y*np.pi/L)

def geoWind(dpdy):
    "The geostrophic wind as a function of pressure gradient"
    return -dpdy / (rho*f)









    

